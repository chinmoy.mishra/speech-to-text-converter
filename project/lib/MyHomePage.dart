import 'dart:convert';

import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:google_translator/google_translator.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String text = "Tap to mic";
  bool isListening = false;
  String translatedText = "";
  String selectedText = "";
  SpeechToText speechToText = SpeechToText();

  @override
  void initState() {
    super.initState();
  }

  Future translate() async {
    const apiKey = 'AIzaSyCqUQPhrjLv-pRKhBrls64v5h0gPDG-n5U';
    const to = 'bn-IN';
    final url = Uri.parse(
        'https://translation.googleapis.com/language/translate/v2?target=$to&key=$apiKey&q=$text');

    final response = await http.post(url);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      List translations = body['data']['translations'];
      String translation = translations.first['translatedText'];
      setState(() {
        translatedText = translation;
        debugPrint(translatedText);
      });
    }
  }

  Future translation() async {
    const apiKey = 'AIzaSyCqUQPhrjLv-pRKhBrls64v5h0gPDG-n5U';
    const to = 'en-IN';
    final url = Uri.parse(
        'https://translation.googleapis.com/language/translate/v2?target=$to&key=$apiKey&q=$selectedText');

    final response = await http.post(url);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      List translations = body['data']['translations'];
      String translation = translations.first['translatedText'];
      setState(() {
        translatedText = translation;
        debugPrint(translatedText);
      });
    }
  }

  Future getData() async {}
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurpleAccent,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              child: SelectableText(
                onSelectionChanged: (selection, cause) {
                  selectedText = text.substring(selection.start, selection.end);
                  print(selectedText);
                },
                contextMenuBuilder: (BuildContext context,
                    EditableTextState editableTextState) {
                  final List<ContextMenuButtonItem> buttonItems =
                      editableTextState.contextMenuButtonItems;

                  buttonItems.insert(
                    buttonItems.length,
                    ContextMenuButtonItem(
                      label: 'translator',
                      onPressed: () {
                        translation();
                      },
                    ),
                  );

                  return AdaptiveTextSelectionToolbar.buttonItems(
                    anchors: editableTextState.contextMenuAnchors,
                    buttonItems: buttonItems,
                  );
                },
                text,
                style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
              ),
            ),
            const Divider(
              thickness: 3,
              color: Colors.blueAccent,
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              child: SelectableText(
                translatedText,
                style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: AvatarGlow(
            animate: isListening,
            endRadius: 75.0,
            glowColor: Theme.of(context).indicatorColor,
            duration: const Duration(
              milliseconds: 2000,
            ),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            child: GestureDetector(
              onTapDown: (details) async {
                if (!isListening) {
                  bool available = await speechToText
                      .initialize(options: [SpeechToText.androidIntentLookup]);
                  if (available) {
                    setState(() {
                      isListening = true;
                      speechToText.listen(
                        listenFor: const Duration(minutes: 5),
                        partialResults: true,
                        onResult: (result) {
                          text = result.recognizedWords;
                          debugPrint(text);
                        },
                      );
                    });
                  }
                }
              },
              onTapUp: (details) {
                setState(() {
                  isListening = false;
                });
                speechToText.stop();
                translatedText = "";
                translate();
              },
              child: CircleAvatar(
                backgroundColor: Colors.deepPurpleAccent,
                radius: 35,
                child: Icon(
                  isListening ? Icons.mic : Icons.mic_off,
                  color: Colors.white,
                ),
              ),
            )),
      ),
    );
  }
}
