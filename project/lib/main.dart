import 'package:flutter/material.dart';
import 'package:google_translator/google_translator.dart';
import 'package:project/MyHomePage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  final String apiKey = "AIzaSyDYCGYXchf2bb_nOHSmWcxdKpeghM3azYE";
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GoogleTranslatorInit(
      apiKey,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
      translateFrom: Locale('en', 'IN'),
      translateTo: Locale('hi', 'IN'),
    );
  }
}
